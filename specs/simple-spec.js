describe('Simple', function(){

    let simple = (a) => {
        return (b) => {
            return a + b
        }
    }

    describe('Simple Function', function(){

        it('should be defined', () => {
            let expectResult = 2;
            let plush = simple(1)
            expect(plush(1)).toBe(expectResult);
        });

    });

})
