import {arrayToStyle} from '../../src/lib/style'

describe('Style', function() {

    it('should return text style by array', () => {
        let expectResult = ' ben and pha'
        let style = arrayToStyle([
            ['ben', true],
            ['and', true],
            ['eiei', false],
            ['pha', true]
        ])
        expect(expectResult).toBe(style)
    });

})
