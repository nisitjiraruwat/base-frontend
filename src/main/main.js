import _ from 'lodash'
import React from 'react'
import Textarea from 'react-textarea-autosize';

/**
    * Material UI
    */
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

import {myCookie} from './../lib/cookie'
import * as MyAjax from './../lib/my-ajax'
import {MeterialMixin} from './../lib/my-meterial-ui'
import {ReactorMixin} from './../lib/reactor'
import {router} from './../lib/router'
//import UploadFileMenu from './../my-button/upload-file-menu'
import {arrayToStyle} from './../lib/style'

export default React.createClass({
    mixins: [MeterialMixin, ReactorMixin],
    getInitialState() {
        /*myCookie.hasCookie('main')
        if(location.pathname === '/') {
            location.replace('/groups')
        }*/
        return {}
    },
    componentWillMount() {
        this.on(MyAjax.logoutSuccess, (response) => {
            myCookie.remove()
            router.navigate('login')
        })
    },
    handleLogout(e) {
        MyAjax.logoutRequestSubject.next('logout')
    },
    render() {
        return (
        <section className='main-container'>
            <AppBar
                title="Title"
                iconElementLeft={<span></span>}
                iconElementRight={
                    <IconMenu
                            iconButtonElement={
                                <IconButton>
                                    <MoreVertIcon />
                                </IconButton>
                            }
                            targetOrigin={{horizontal: 'right', vertical: 'top'}}
                            anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                        >
                        <MenuItem primaryText="Logout" onMouseDown={this.handleLogout} />
                  </IconMenu>
                }
                />
                <section className='row-flex'>

                </section>
        </section>
        );
    }
})
/*<Queue/>*/
