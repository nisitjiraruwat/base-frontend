import _ from 'lodash'
import React from 'react'

/**
    * Material UI
    */
import RaisedButton from 'material-ui/RaisedButton'
import {Card, CardActions, CardTitle, CardText} from 'material-ui/Card'
import TextField from 'material-ui/TextField'

import {myCookie} from './../lib/cookie'
import * as MyAjax from './../lib/my-ajax'
import {MeterialMixin} from './../lib/my-meterial-ui'
import {ReactorMixin} from './../lib/reactor'
import {router} from './../lib/router'

let LoginForm = React.createClass({
    mixins: [ReactorMixin],
    getInitialState() {
        return {email: '', password: ''};
    },
    componentWillMount() {
        this.on(MyAjax.loginSuccess, (response) => {
            myCookie.save(_.merge(response.data, {
                token: response.token.split(' ')[1].split('>')[0]
            }))
            router.navigate('home')
        })
    },
    handleChange(state) {
        return (e) => {
            let setState = {}
            setState[`${state}`] = e.target.value
            this.setState(setState)
        }
    },
    handleSubmit(e) {
        e.preventDefault();
        let email = this.state.email.trim();
        let password = this.state.password.trim();
        if (!email || !password) {
          return;
        }

        MyAjax.loginRequestSubject.next({email: email, password: password})
    },
    toSignUp(e) {
        router.navigate('signup')
    },
    render() {
        return (
            <form className='form-content' onSubmit={this.handleSubmit}>
                <TextField
                    floatingLabelText='Email'
                    value={this.state.email}
                    onChange={this.handleChange('email')}
                    />
                <TextField
                    floatingLabelText='Password'
                    value={this.state.password}
                    onChange={this.handleChange('password')}
                    type="password"
                    />
                <CardActions className='row-content'>
                    <RaisedButton
                        label='Login'
                        className="mdl-button-50-per" primary={true} type="submit">
                    </RaisedButton>
                    <RaisedButton
                        label='Sign Up'
                        className="mdl-button-50-per" primary={true} onMouseDown={this.toSignUp}>
                    </RaisedButton>
                </CardActions>
            </form>
        );
    }
});

export default React.createClass({
    mixins: [MeterialMixin],
    render() {
        return (
            <section className='login-container'>
                <Card>
                    <CardTitle><span className='title'>Login</span></CardTitle>
                    <CardText>
                        <LoginForm />
                    </CardText>
                </Card>
            </section>
        );
    }
})
