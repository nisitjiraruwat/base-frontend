import React from 'react'

/**
    * Material UI
    */
import IconButton from 'material-ui/IconButton'

import * as MyAjax from './../lib/my-ajax'
//import {myCookie} from './../lib/cookie'
/*import {socket} from './../lib/notification'

let updateMemberProfile = (id) => {
    socket.emit('updateMemberProfile', {id: id});
}*/

let updateImage = (image, url, myRX) => {
    let formData = MyAjax.getFormData({
        'image': image
    })
    return MyAjax.ajax({
            processData: false,
            contentType: false,
            cache: false,
            url: `${url}`,
            type: 'POST',
            data: formData
        }, true)
        .then((data) => {
            //myCookie.save(data)
            //updateMemberProfile(myCookie.load().id)
            myRX.next('success')
            return data
        })
}
//url: `/b/queuez/updatephotomember/${idname}/`,
export default React.createClass({
    handleFileChange(e) {
        updateImage(e.target.files[0], this.props.url, this.props.myRX)
    },
    render() {
        return (
            <IconButton style={{width: 36, height: 36, padding: 8}}
                className="upload-file-button-container">
                {this.props.icon}
                <label className='button' htmlFor={this.props.id}>
                </label>
                <input className='none' id={this.props.id} name='file' type='file'
                    onChange={this.handleFileChange}
                />
            </IconButton>
        )
    }
})
