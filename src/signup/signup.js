import _ from 'lodash'
import React from 'react'

/**
    * Material UI
    */
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton'
import RaisedButton from 'material-ui/RaisedButton'
import {Card, CardActions, CardTitle, CardText} from 'material-ui/Card'
import TextField from 'material-ui/TextField'

import {myCookie} from './../lib/cookie'
import * as MyAjax from './../lib/my-ajax'
import UploadFileButton from './../my-button/upload-file-button'
import {MeterialMixin} from './../lib/my-meterial-ui'
import {ReactorMixin} from './../lib/reactor'
import {router} from './../lib/router'

let SignUpForm = React.createClass({
    mixins: [ReactorMixin],
    getInitialState() {
        return {email: '', password: '', confirmPassword: '', name: '', gender: 'true'}
    },
    componentWillMount() {
        this.on(MyAjax.signupSuccess, (response) => {
            router.navigate('login')
        })
    },
    handleChange(state) {
        return (e) => {
            let setState = {}
            setState[`${state}`] = e.target.value
            this.setState(setState)
        }
    },
    handleFileChange(value) {
        this.setState({file: value})
    },
    handleSubmit(e) {
        e.preventDefault()
        let email = this.state.email.trim()
        let password = this.state.password.trim()
        let confirmPassword = this.state.confirmPassword.trim()
        let name = this.state.name.trim()
        let file = this.state.file
        if (!email || !password || !confirmPassword || !name || !file) {
            return
        }
        MyAjax.signupRequestSubject.next({
            email: email,
            password: password,
            name: name,
            image: file,
            gender: this.state.gender.trim() == 'true'
        })
    },
    isSignUp() {
        let email = this.state.email.trim()
        let password = this.state.password.trim()
        let confirmPassword = this.state.confirmPassword.trim()
        let name = this.state.name.trim()
        let file = this.state.file
        return email && password && confirmPassword && name && file
            && password === confirmPassword
    },
    goToLogin(e) {
        router.navigate('login')
    },
    render() {
        return (
            <form className='form-content' onSubmit={this.handleSubmit}>
                <TextField
                    floatingLabelText='Email'
                    value={this.state.email}
                    onChange={this.handleChange('email')}
                    />
                <TextField
                    floatingLabelText='Password'
                    value={this.state.password}
                    onChange={this.handleChange('password')}
                    type='password'
                    />
                <TextField
                    floatingLabelText='Confirm Password'
                    value={this.state.confirmPassword}
                    onChange={this.handleChange('confirmPassword')}
                    type='password'
                    />
                <TextField
                    floatingLabelText='Name'
                    value={this.state.name}
                    onChange={this.handleChange('name')}
                    />
                <RadioButtonGroup className='row-content radio'
                    name='gender' defaultSelected='true'
                    onChange={this.handleChange('gender')}>
                    <RadioButton
                        style={{maxWidth: '50%'}}
                        value='true'
                        label='Male'
                        />
                    <RadioButton
                        style={{maxWidth: '50%'}}
                        value='false'
                        label='Female'
                        />
                </RadioButtonGroup>
                <div className='row-content'>
                    <img id='image' className='profile' src='/media/users/not-img.png'/>
                </div>
                <div className='row-content'>
                    <UploadFileButton id='upload-file' tooltip='Upload image'
                        touch={true} tooltipPosition='top-center'
                        imageId='image'
                        onFileChange={this.handleFileChange}
                    />
                </div>
                <CardActions className='row-content'>
                    <RaisedButton
                        label='Sign Up'
                        primary={true} className='mdl-button-50-per' type='submit'
                        disabled={!this.isSignUp()}>
                    </RaisedButton>
                    <RaisedButton
                        label='Back'
                        primary={true} className='mdl-button-50-per' onMouseDown={this.goToLogin} >
                    </RaisedButton>
                </CardActions>
            </form>
        )
    }
})

export default React.createClass({
    mixins: [MeterialMixin],
    getInitialState() {
        myCookie.hasCookie('login')
        return {}
    },
    render() {
        return (
            <section className='sign-up-container'>
                <Card>
                    <CardTitle><span className='title'>Sign In</span></CardTitle>
                    <CardText>
                        <SignUpForm />
                    </CardText>
                </Card>
            </section>
        )
    }
})
