import _ from 'lodash'
import cookie from 'react-cookie'

let keyCookie = 'base-ck' // Cookie name

let myCookie = {
    load() {
        return cookie.load(keyCookie)
    },
    save(data) {
        let myCookie = {}
        if(cookie.load(keyCookie)) {
            myCookie = cookie.load(keyCookie)
        }
        cookie.save(keyCookie, _.merge(myCookie, data), { path: '/' , expires: new Date(new Date().setYear(new Date().getFullYear() + 1))})
    },
    remove() {
        cookie.remove(keyCookie, { path: '/' })
        cookie.remove('csrftoken', { path: '/' })
    },
    hasCookie(state) {
        if(!cookie.load(keyCookie) && state === 'main') {
            location.replace(`${location.protocol}//${location.host}/login`)
        } else if(cookie.load(keyCookie) && state === 'login') {
            location.replace(`${location.protocol}//${location.host}/`)
        }
    }
}

export {
    myCookie
}
