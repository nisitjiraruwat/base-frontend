import _ from 'lodash'
import $ from 'jquery'
import cookie from 'react-cookie'
import Rx from '@reactivex/rxjs'

import {myCookie} from './cookie'

let ajax = (options, isAuthorization) => {
    let defaultOptions = {
        contentType: 'application/json',
        dataType: 'json',
        beforeSend (xhr) {
            if(isAuthorization === true) {
                xhr.setRequestHeader('Authorization', 'Token ' + myCookie.load().token)
                xhr.setRequestHeader('X-CSRFToken', cookie.load('csrftoken'))
            }
        },
    }
    let newOptions = _.merge(defaultOptions, options)
    return $.ajax(newOptions)
}

let objectToFormData = (data) => {
    let fd = new FormData();
    _.forEach(data, function(value, key) {
        fd.append(key, value);
    });
    return fd;
}

/*
    * Signup
    */
let signup = (data) => {
    return Rx.Observable.create(observer => {
        let formData = objectToFormData(data)
        return ajax({
            processData: false,
            contentType: false,
            cache: false,
            url: `b/app/signup/`,
            type: 'POST',
            data: formData
        })
        .then((data, status) => {
            debugger
            data.status = status
            observer.next(data)
            observer.complete()
        })
    })
}

const signupRequestSubject = new Rx.Subject()
const signupResponse = signupRequestSubject
    .switchMap(signup)
    .share()

const signupSuccess = signupResponse
    .filter((response) => response.status === 'success')

/*
    * Login
    */

let login = (data) => {
    return Rx.Observable.create(observer => {
        ajax({
            url: `b/app/login/`,
            type: 'POST',
            data: JSON.stringify({
                email: data.email,
                password: data.password
            })
        })
        .then((data, status) => {
            data.status = status
            observer.next(data)
            observer.complete()
        })
    })
}

const loginRequestSubject = new Rx.Subject()
const loginResponse = loginRequestSubject
    .switchMap(login)
    .share()

const loginSuccess = loginResponse
    .filter((response) => response.status === 'success')

/*
    * Logout
    */

const logout = () => {
    return Rx.Observable.create(observer => {
        $.get({
            url: `b/app/logout/`
        })
        .then((data, status) => {
            observer.next({status: status})
            observer.complete()
        })
    })
}
const logoutRequestSubject = new Rx.Subject()
const logoutResponse = logoutRequestSubject
    .filter((status) => status === 'logout')
    .switchMap(logout)
    .share()

const logoutSuccess = logoutResponse
    .filter((response) => response.status === 'success')

export {
    ajax,
    signupRequestSubject,
    signupSuccess,
    loginRequestSubject,
    loginSuccess,
    logoutRequestSubject,
    logoutSuccess,
    objectToFormData
}

/*
n searchWikipedia (term) {
    var promise = $.ajax({
        url: 'http://en.wikipedia.org/w/api.php',
        dataType: 'jsonp',
        data: {
            action: 'opensearch',
            format: 'json',
            search: encodeURI(term)
        }
    }).promise();
    return Rx.Observable.fromPromise(promise);
*/
