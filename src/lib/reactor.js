const ReactorMixin = {
    componentWillMount() {
        this.subscriptions = []
    },
    on(observable, reaction) {
        this.subscriptions.push(observable.subscribe(reaction))
    },
    componentWillUnmount() {
        this.subscriptions.forEach(s => s.unsubscribe())
    }
}

export {
    ReactorMixin
}
