import Rx from '@reactivex/rxjs'
import {Router5} from 'router5'
import listenersPlugin from 'router5-listeners'
import historyPlugin from 'router5-history'

import {myCookie} from './cookie'

const router = new Router5(routes, {
        useHash: false,
        hashPrefix: '',
        defaultRoute: 'home',
        defaultParams: {},
        base: '',
        trailingSlash: false,
        autoCleanUp: true,
        strictQueryParams: true
    })
router.add({ name: 'home', path: '/' })
router.add({ name: 'login', path: '/login' })
router.add({ name: 'signup', path: '/signup' })

router.usePlugin(listenersPlugin())
router.usePlugin(historyPlugin())

// prevent unauthenticated user from auth zone
router.canActivate('home', (toState, fromState) => {
    if(!myCookie.load()) {
        return Promise.reject({ redirect: { name: 'login' } })
    } else {
        return true
    }
})

router.canActivate('login', () => {
    if(myCookie.load()) {
        return Promise.reject({ redirect: { name: 'home' } })
    } else {
        return true
    }
})

const routeActivatedSubject = new Rx.Subject()
router.addListener((to, from) => {
    routeActivatedSubject.next({to, from})
})

const routeActivated = routeName => {
    return routeActivatedSubject.filter(() => {
        return router.isActive(routeName)
    }).asObservable()
}

const routes = {
    home: {
        activated: routeActivated('home')
    },
    login: {
        activated: routeActivated('login')
    },
    signup: {
        activated: routeActivated('signup')
    }
}

export {
    router,
    routes
}
