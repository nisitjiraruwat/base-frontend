const webpack = require('webpack');
const path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin');

const src = path.join(__dirname, 'src');
const dist = path.join(__dirname, 'dist/js');
const CommonsChunkPlugin = webpack.optimize.CommonsChunkPlugin;

module.exports = {
    context: src,
    entry: {
        app: path.join(src, 'app.js'),
        react: ['react', '@reactivex/rxjs', 'jquery', 'material-ui'
            , 'react-cookie', 'react-dom', 'react-tap-event-plugin'
            , 'react-textarea-autosize', 'router5', 'router5-history'
            , 'router5-listeners'],
        //angular: [path.join(__dirname, 'node_modules/react-mdl/extra/material.min.js')]
    },
    output: {
        path: dist,
        filename: '[name].js'
    },
    module: {
        loaders: [{
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel',
                query: {
                    presets: ['es2015', 'react']
                }
            }
        ]
    },
    resolve: {
        extensions: ["", ".js", ".es6"]
    },
    devtool: 'source-map',
    plugins: [
        new CleanWebpackPlugin(['dist/js', 'dist/index.html']),
        new CommonsChunkPlugin({names: ['react'], filename: '[name].[hash].js'}),
        new HtmlWebpackPlugin({
            filename: path.join(__dirname, 'dist/index.html'),
            template: 'index.html'
        })
    ]
};
